function mhcode_bootstrapCompareFrac(cx_data1,cx_data2,cx_nbtstrp,cx_str)
    % cx_data1,2 is ndatapointsx2, sum(col1)/sum(col2) is the target variable
    
    if nargin<3
        cx_nbtstrp=1000;
    end
    if nargin<4
        cx_str='';
    end

       
    cx_boot1 = bootstrp(cx_nbtstrp,@cx_getFrac,cx_data1(:,1),cx_data1(:,2));
    cx_frac1 = cx_getFrac(cx_data1(:,1),cx_data1(:,2));
    figure,histogram(cx_boot1),hold on
    
    cx_boot2 = bootstrp(cx_nbtstrp,@cx_getFrac,cx_data2(:,1),cx_data2(:,2));
    cx_frac2 = cx_getFrac(cx_data2(:,1),cx_data2(:,2));
    histogram(cx_boot2),hold on;
    
    cx_comp = double(repmat(cx_boot1,[1 cx_nbtstrp]) >=  repmat(cx_boot2',[cx_nbtstrp 1]));
    cx_leq = sum(cx_comp(:));
    
    title(sprintf('%s\n%.2f, n=%d vs %.2f, n=%d; bootstrap:\n%.2f +- %.2f vs %.2f +- %.2f\nLargerequal: %d of %d (p=%d)\nSmaller: %d (p=%d)',cx_str,cx_frac1,size(cx_data1,1),cx_frac2,size(cx_data2,1),...
        mean(cx_boot1),std(cx_boot1),mean(cx_boot2),std(cx_boot2),...
        cx_leq,cx_nbtstrp^2,cx_leq/cx_nbtstrp^2,cx_nbtstrp^2-cx_leq,(cx_nbtstrp^2-cx_leq)/cx_nbtstrp^2));



end