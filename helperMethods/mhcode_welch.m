function [t,p] = mhcode_welch(mean1,mean2,sem1,sem2,n1,n2)

    cx_scomb = sqrt(sem1^2+sem2^2);
    
    cx_t = abs(mean1-mean2)/cx_scomb;
    cx_df = (sem1^2+sem2^2)^2/...
        (sem1^4/(n1-1) + sem2^4/(n2-1));
    cx_p = 1-tcdf(cx_t,cx_df);

    t=cx_t;
    p=cx_p;






end