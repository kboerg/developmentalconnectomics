function createResolutionPyramid(inParam, bbox, outRoot, isSeg, voxelSize)
%Input:
%   If using just one input argument, and normal wK hierachy with JSONs
%   already present, rest inferred.
%   param = Parameter struct for mag1 KNOSSOS or WKW dataset
%   bbox = Bounding Box of KNOSSOS Hierachy mag1 
%       (minimal coordinates should align with start of Knossos cube (e.g. [1 1 1]))
%       (Not sure whether it will work if lower corner in bbox is not [1 1 1], NOT tested)
%   outRoot = where to write higher resolutions (subfolder named as the magnification will be created inside)
%   subsamplingSegmentation = if you try to subsample a segmentation (instead of raw data)
%       set this to true, will use @mode instead of @median for subsampling and some other tricks
%       THIS WILL REQUIRE MORE THAN 12GB of RAM with current settings (48GB works). Set your IndependentSubmitFunction!
%
% Written by
%   Manuel Berning <manuel.berning@brain.mpg.de>
%   Alessandro Motta <alessandro.motta@brain.mpg.de>

if nargin < 4 || isempty(isSeg)
    isSeg = false;
end

if ~exist('voxelSize', 'var')
    voxelSize = [1, 1, 1];
end

if isSeg
    gbRamPerTask = 48;
    dtype = 'uint32';
else
    gbRamPerTask = 12;
    dtype = 'uint8';
end

if ~isfield(inParam, 'dtype')
    inParam.dtype = dtype;
end

% The root directory specified in inParam might just be a symbolic link. It
% is thus possible, that the mag1 subdirectory is hidden. To avoid this,
% use readlink to resolve the symbolic link.
[~, inParam.root] = system(sprintf( ...
    'readlink -f "%s" < /dev/null', inParam.root));
inParam.root = strcat(strtrim(inParam.root), filesep);
assert(exist(inParam.root, 'dir') ~= 0);

% Make sure input is mag1
if isfield(inParam, 'prefix') && ...
        isempty(regexp(inParam.prefix, '_mag1$', 'once'))
    error('Input parameters must be for mag1');
end

if nargin < 3 || isempty(outRoot)
    dirParts = strsplit(inParam.root, filesep);
    
    % handle case of trailing filesep
    if isempty(dirParts{end}); dirParts(end) = []; end
    
    % go one level up
    dirParts(end) = [];
    outRoot = strjoin(dirParts, filesep);
    
    % read- / writeKnossosRoi expect the root to end with a filesep
    outRoot(end + 1) = filesep;
    clear('dirParts');
    Util.log('Output is written to %s.', outRoot);
end

% build parameters
outParam = inParam;
outParam.root = outRoot;

if nargin < 2 || isempty(bbox)
    % Get bounding box from section.json
    temp = readJson(fullfile(outParam.root, 'section.json'));
    bbox = double(cell2mat(cat(2, temp.bbox{:}))');
    clear temp;
    
    % convert to MATLAB style
    assert(all(bbox(:) >= 0));
    bbox(:, 1) = bbox(:, 1) + 1;
    Util.log('Bbox was set to %s.', mat2str(bbox));
else
    bbox = double(bbox);
end

% Set according to memory limits, currently optimized for 12 GB RAM,
% segmentation will need 48 GB currently
% Will be paralellized on cubes of this size
cubeSize = [1024, 1024, 1024];

% Determine which magnifications to write
assert(voxelSize(1) == voxelSize(2));
magsToWrite = [1, 1, 1];

while magsToWrite(end, 3) < 512
    curVoxelSize = magsToWrite(end, :) .* voxelSize;
    if 2 * curVoxelSize(1) < curVoxelSize(3)
        % NOTE(amotta): At this point the in-plane pixel size is still
        % very much smaller than the cutting thickness. To reduce the
        % anisotropy let's downsample only within XY plane.
        curPoolVol = [2, 2, 1];
    else
        % NOTE(amotta): The voxel size is now roughly isotropic, so
        % downsample equally in all three dimensions.
        curPoolVol = [2, 2, 2];
    end

    magsToWrite(end + 1, :) = ...
        magsToWrite(end, :) .* curPoolVol; %#ok
    clear curVoxelSize curPoolVol;
end

% Remove `1-1-1` from list
magsToWrite(1, :) = [];

magGroups = {[1, 1, 1]};
for curIdx = 1:size(magsToWrite)
    curMag = magsToWrite(curIdx, :);

    if any(curMag ./ magGroups{end}(1, :) > 8)
        magGroups{end + 1} = vertcat( ...
            magGroups{end}(end, :), curMag); %#ok
    else
        magGroups{end} = vertcat( ...
            magGroups{end}, curMag);
    end
end

% Create output folder if it does not exist
if ~exist(outParam.root, 'dir')
    mkdir(outParam.root);
end

% Initialize wkw datasets
if isfield(outParam, 'backend') ...
        && strcmp(outParam.backend, 'wkwrap')
    allMagRoots = cellfun( ...
        @(magIds) sprintf('%d-%d-%d', magIds), ...
        num2cell(magsToWrite, 2), 'UniformOutput', false);
    allMagRoots = fullfile(outParam.root, allMagRoots);
    
    cellfun(@(curRoot) wkwInit( ...
        'new', curRoot, 32, 32, outParam.dtype, 1), allMagRoots);
    clear allMagRoots;
end

% Do the work, submitted to scheduler
curBbox = bbox;
curInParam = inParam;

for i = 1:numel(magGroups)
    X = unique([curBbox(1, 1):cubeSize(1):curBbox(1, 2), curBbox(1, 2)]);
    Y = unique([curBbox(2, 1):cubeSize(2):curBbox(2, 2), curBbox(2, 2)]);
    Z = unique([curBbox(3, 1):cubeSize(3):curBbox(3, 2), curBbox(3, 2)]);
    
    idx = 1;
    inputCell = cell(prod(cellfun(@numel, {X, Y, Z}) - 1), 1);
    
    for x = 1:(numel(X) - 1)
        for y = 1:(numel(Y) - 1)
            for z = 1:(numel(Z) - 1)
                thisBBox = [ ...
                    X(x), (X(x + 1) - 1);
                    Y(y), (Y(y + 1) - 1);
                    Z(z), (Z(z + 1) - 1)];
                inputCell{idx} = { ...
                    curInParam, thisBBox, magGroups{i}, outParam, isSeg};
                idx = idx + 1;
            end
        end
    end
    
    job = Cluster.startJob( ...
        @writeSupercubes, inputCell, ...
        'cluster', {'memory', gbRamPerTask}, ...
        'name', 'supercubes');
    Cluster.waitForJob(job);
    
    % Update bounding box
    curMag = magGroups{i}(end, :);
    curBbox = ceil(bsxfun(@rdivide, bbox - 1, curMag(:))) + 1;
    
    % Update root path
    curInParam.root = fullfile( ...
        outParam.root, sprintf('%d-%d-%d', curMag));
    curInParam.root(end + 1) = filesep;
    
    % Update prefix
    if isfield(curInParam, 'prefix')
        curInParam.prefix = regexprep( ...
            curInParam.prefix, '(\d+)$', num2str(curMag));
    end
end

end
