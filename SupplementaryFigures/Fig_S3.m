%% Figure S3 new - Mulitplicity 21.01.2021
%read from excel file for multiplicity and innervation
cx_data = xlsread('L4_RawData_16-07-2020_corr_21-01-2021.xlsx','Sheet3');
% soma axons
ToSel_1 = find(cx_data(:,1)== 7& cx_data(:,3)== 3);
P7inn = cx_data(ToSel_1,8);
P7avgSyn = cx_data(ToSel_1,7);
P7somSyn = cx_data(ToSel_1,5);
P7all = cat(2,P7somSyn,P7inn,P7avgSyn);
Thr_P7 = find(P7all(:,1) < 2); % set thr for > 1 syn
P7thr = P7all;
P7thr(P7thr(:,1) < 2, :) = [];%delete thr rows
P7avgSyn_noise = P7thr(:,3)+(rand(size(P7thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target 0.25 earlier noise

ToSel_2a = find(cx_data(:,1)== 9.1& cx_data(:,3)== 3);
ToSel_2b = find(cx_data(:,1)== 9.2& cx_data(:,3)== 3);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
P9inn = cx_data(ToSel_2,8);
P9avgSyn = cx_data(ToSel_2,7);
P9somSyn = cx_data(ToSel_2,5);
P9all = cat(2,P9somSyn,P9inn,P9avgSyn);
Thr_P9 = find(P9all(:,1) < 2); % set thr for > 1 syn
P9thr = P9all;
P9thr(P9thr(:,1) < 2, :) = [];%delete thr rows
P9avgSyn_noise = P9thr(:,3)+(rand(size(P9thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target

ToSel_3a = find(cx_data(:,1)== 14.1& cx_data(:,3)== 3);
ToSel_3b = find(cx_data(:,1)== 14.2& cx_data(:,3)== 3);
ToSel_3 = cat(1,ToSel_3a,ToSel_3b);
P14inn = cx_data(ToSel_3,8);
P14avgSyn = cx_data(ToSel_3,7);
P14somSyn = cx_data(ToSel_3,5);
P14all = cat(2,P14somSyn,P14inn,P14avgSyn);
Thr_P14 = find(P14all(:,1) < 2); % set thr for > 1 syn
P14thr = P14all;
P14thr(P14thr(:,1) < 2, :) = [];%delete thr rows
P14avgSyn_noise = P14thr(:,3)+(rand(size(P14thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target

ToSel_4 = find(cx_data(:,1)== 28& cx_data(:,3)== 3);
P28inn = cx_data(ToSel_4,8);
P28avgSyn = cx_data(ToSel_4,7);
P28somSyn = cx_data(ToSel_4,5);
P28all = cat(2,P28somSyn,P28inn,P28avgSyn);
Thr_P28 = find(P28all(:,1) < 2); % set thr for > 1 syn
P28thr = P28all;
P28thr(P28thr(:,1) < 2, :) = [];%delete thr rows
P28avgSyn_noise = P28thr(:,3)+(rand(size(P28thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target 0.25 earlier noise

% Plot
figure
subplot(2,2,1)
scatter(P7thr(:,2),P7avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P7')

subplot(2,2,2)
scatter(P9thr(:,2),P9avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P9')

subplot(2,2,3)
scatter(P14thr(:,2),P14avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P14')

subplot(2,2,4)
scatter(P28thr(:,2),P28avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of SM')
ylabel('average # syn per SM (per axon)')
title('P28')

%% %% AD axons mulitpicity
clear all
cx_data = xlsread('L4_RawData_16-07-2020_corr_21-01-2021.xlsx','Sheet3');
ToSel_1 = find(cx_data(:,1)== 7& cx_data(:,3)== 1);
P7inn = cx_data(ToSel_1,8);
P7avgSyn = cx_data(ToSel_1,7);
P7ADsyn = cx_data(ToSel_1,5);
P7all = cat(2,P7ADsyn,P7inn,P7avgSyn);
Thr_P7 = find(P7all(:,1) < 2); % set thr for > 1 syn
P7thr = P7all;
P7thr(P7thr(:,1) < 2, :) = [];%delete thr rows
P7avgSyn_noise = P7thr(:,3)+(rand(size(P7thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target 0.25 earlier noise
 
ToSel_2a = find(cx_data(:,1)== 9.1& cx_data(:,3)== 1);
ToSel_2b = find(cx_data(:,1)== 9.2& cx_data(:,3)== 1);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
P9inn = cx_data(ToSel_2,8);
P9avgSyn = cx_data(ToSel_2,7);
P9ADsyn = cx_data(ToSel_2,5);
P9all = cat(2,P9ADsyn,P9inn,P9avgSyn);
Thr_P9 = find(P9all(:,1) < 2); % set thr for > 1 syn
P9thr = P9all;
P9thr(P9thr(:,1) < 2, :) = [];%delete thr rows
P9avgSyn_noise = P9thr(:,3)+(rand(size(P9thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target
 
ToSel_3a = find(cx_data(:,1)== 14.1& cx_data(:,3)== 1);
ToSel_3b = find(cx_data(:,1)== 14.2& cx_data(:,3)== 1);
ToSel_3 = cat(1,ToSel_3a,ToSel_3b);
P14inn = cx_data(ToSel_3,8);
P14avgSyn = cx_data(ToSel_3,7);
P14ADsyn = cx_data(ToSel_3,5);
P14all = cat(2,P14ADsyn,P14inn,P14avgSyn);
Thr_P14 = find(P14all(:,1) < 2); % set thr for > 1 syn
P14thr = P14all;
P14thr(P14thr(:,1) < 2, :) = [];%delete thr rows
P14avgSyn_noise = P14thr(:,3)+(rand(size(P14thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target
 
ToSel_4 = find(cx_data(:,1)== 28& cx_data(:,3)== 1);
P28inn = cx_data(ToSel_4,8);
P28avgSyn = cx_data(ToSel_4,7);
P28ADsyn = cx_data(ToSel_4,5);
P28all = cat(2,P28ADsyn,P28inn,P28avgSyn);
Thr_P28 = find(P28all(:,1) < 2); % set thr for > 1 syn
P28thr = P28all;
P28thr(P28thr(:,1) < 2, :) = [];%delete thr rows
P28avgSyn_noise = P28thr(:,3)+(rand(size(P28thr(:,3),1),1)*0.05);% add noise to y-axis of avg syn per target 0.25 earlier noise
 
% Plot
figure
subplot(2,2,1)
scatter(P7thr(:,2),P7avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per SM (per axon)')
title('P7')
 
subplot(2,2,2)
scatter(P9thr(:,2),P9avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per SM (per axon)')
title('P9')
 
subplot(2,2,3)
scatter(P14thr(:,2),P14avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per SM (per axon)')
title('P14')
 
subplot(2,2,4)
scatter(P28thr(:,2),P28avgSyn_noise,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',50);
set(gca,'TickDir','out')
box off
set(gca,'Ylim',[1 8])
set(gca,'Xlim',[0 1])
xlabel('Fractional innervation of AD')
ylabel('average # syn per SM (per axon)')
title('P28')




