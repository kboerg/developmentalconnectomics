%% FIGURE_05 - L2/3 PLOTS
Age = [9 14 28];

%% L2/3 AIS_input_synapse density
cx_data = xlsread('data/L23_RawData_04-07-2020.xlsx','Sheet2');
ToSel_1 = find(cx_data(:,1)== 9); 
pL9= cx_data(ToSel_1,3);
tSyn9 = cx_data(ToSel_1,9);
P9 = tSyn9./pL9;

ToSel_2 = find(cx_data(:,1)== 14); 
pL14= cx_data(ToSel_2,3);
tSyn14 = cx_data(ToSel_2,9);
P14 = tSyn14./pL14;

ToSel_3 = find(cx_data(:,1)== 28); 
pL28= cx_data(ToSel_3,3);
tSyn28 = cx_data(ToSel_3,9);
P28 = tSyn28./pL28;

AIS = cat(1,P9,P14,P28);
Err_9 = std(P9)/sqrt(numel(P9));
Err_14 = std(P14)/sqrt(numel(P14));
Err_28 = std(P28)/sqrt(numel(P28));
Err = cat(1,Err_9,Err_14,Err_28);
Mean = [mean(P9);mean(P14);mean(P28)];
g1=ones(size(P9)); g2 =2*ones(size(P14)); g3=3*ones(size(P28));
G=cat(1,g1,g2,g3);

%Plotting
figure
boxplot(AIS,G,'Positions',[9 14 28],'Colors','k')
hold on
plot([9 14 28],Mean,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot([9 14 28], (Mean+Err),'--k')
hold on
plot([9 14 28],Mean-Err,'--k')
hold on
scatter(ones(numel(P9),1)*9+rand(numel(P9),1)*.75-.45, P9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P14),1)*14+rand(numel(P14),1)*.75-.45, P14,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P28),1)*28+rand(numel(P28),1)*.75-.45, P28,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
box off
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 9 14 28])
set(gca,'XtickLabel',[0 9 14 28])
set(gca,'TickDir','out')
xlabel('Postnatal ages (days)')
ylabel('Input synapses on AIS (per unit um AIS length')
title('L2/3 - AIS input synapse density')
