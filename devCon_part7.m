%% Final curation Table 1 including statistics: som and AD axons
% data collection

cx_path = 'data'

cx_syndata = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'));
cx_nbootstrp=1000;

cx_syndensAggr=[];
cx_densData = {}

cx_dataExtract = {}

cx_rowranges = {[48:68],[69:82],[160:189],[190:211],[212:224],[409:443],[444:475],[519:538]} % L4 som: p7d1 p7d2 p9d1 p9d2 p9d3 p14d1 p14d2 p28d1
cx_colrange = [9]% 13]    % som and som fil
cx_allsynrange = [7:13]
cx_PLcol = 4;


for i=1:length(cx_rowranges)
    cx_thisrowrange = cx_rowranges{i}; 
    
    cx_thisdata = cx_syndata(cx_thisrowrange,:);
    cx_nOnTargetInclSeed = sum(cx_thisdata(:,cx_colrange),2);
    cx_allsynInclSeed = sum(cx_thisdata(:,cx_allsynrange),2);
    cx_pl = cx_thisdata(:,cx_PLcol);
    
    cx_dataExtract{1,i} = {cx_nOnTargetInclSeed,cx_allsynInclSeed,cx_pl};
end    
    
    % AD
    

cx_rowranges = {[14:33],[34:46],[84:133],[134:142],[143:158],[226:292],[293:341],[478:500]} % L4 AD: p7d1 p7d2 p9d1 p9d2 p9d3 p14d1 p14d2 p28d1
cx_colrange = 7    % AD
cx_allsynrange = [7:13]
cx_PLcol = 4;
    
for i=1:length(cx_rowranges)
    cx_thisrowrange = cx_rowranges{i}; 
    
    cx_thisdata = cx_syndata(cx_thisrowrange,:);
    cx_nOnTargetInclSeed = sum(cx_thisdata(:,cx_colrange),2);
    cx_allsynInclSeed = sum(cx_thisdata(:,cx_allsynrange),2);
    cx_pl = cx_thisdata(:,cx_PLcol);
    
    cx_dataExtract{2,i} = {cx_nOnTargetInclSeed,cx_allsynInclSeed,cx_pl};
end    
    
%% generate table bootstrap comparison numbers and plots

% needs previous section for data collection

% bootstrap direct comparisons:


cx_targetTypes = {'Soma','AD'}

for cx_tc=1:length(cx_targetTypes)
    
    % p7 p9
    cx_cmpdata = [cx_dataExtract{cx_tc,1}{1}-1,cx_dataExtract{cx_tc,1}{2}-1];   %  p7d1
    %cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,2}{1}-1,cx_dataExtract{cx_tc,2}{2}-1];   %  p7d2
    cx_boot1 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac1 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    figure,histogram(cx_boot1),hold on
    
    cx_cmpdata = [cx_dataExtract{cx_tc,3}{1}-1,cx_dataExtract{cx_tc,3}{2}-1];   % p9d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,4}{1}-1,cx_dataExtract{cx_tc,4}{2}-1];   %  p9d2
    %cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,5}{1}-1,cx_dataExtract{cx_tc,5}{2}-1];   %  p9d3
    cx_boot2 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac2 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    histogram(cx_boot2),hold on;
    
    cx_comp = double(repmat(cx_boot1,[1 cx_nbootstrp]) >=  repmat(cx_boot2',[cx_nbootstrp 1]));
    if sum(cx_comp(:))>0
        cx_p = sum(cx_comp(:))/cx_nbootstrp^2;
    else
        cx_p = 1/cx_nbootstrp^2;
    end
    
    title(sprintf('p7 vs p9 %s frac, orig datasets\n %s',...
        cx_targetTypes{cx_tc},...
        sprintf('%.4f (%.4f) +- %.4f %% vs %.4f (%.4f) +- %.4f %%, p=%d (%d)',mean(cx_boot1)*100,cx_frac1*100,std(cx_boot1)*100,mean(cx_boot2)*100,cx_frac2*100,std(cx_boot2)*100,cx_p,1-cx_p)));

    
    % p9 p14 som
    cx_cmpdata = [cx_dataExtract{cx_tc,3}{1}-1,cx_dataExtract{cx_tc,3}{2}-1];   %  p9d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,4}{1}-1,cx_dataExtract{cx_tc,4}{2}-1];   %  p9d2
    cx_boot1 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac1 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    figure,histogram(cx_boot1),hold on
    
    cx_cmpdata = [cx_dataExtract{cx_tc,6}{1}-1,cx_dataExtract{cx_tc,6}{2}-1];   %  p14d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,7}{1}-1,cx_dataExtract{cx_tc,7}{2}-1];   %  p14d2
    cx_boot2 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac2 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    histogram(cx_boot2),hold on;
    
    cx_comp = double(repmat(cx_boot1,[1 cx_nbootstrp]) >=  repmat(cx_boot2',[cx_nbootstrp 1]));
    if sum(cx_comp(:))>0
        cx_p = sum(cx_comp(:))/cx_nbootstrp^2;
    else
        cx_p = 1/cx_nbootstrp^2;
    end
    
    title(sprintf('p9 vs p14 %s frac, orig datasets\n %s',...
        cx_targetTypes{cx_tc},...
        sprintf('%.4f (%.4f) +- %.4f %% vs %.4f (%.4f) +- %.4f %%, p=%d (%d)',mean(cx_boot1)*100,cx_frac1*100,std(cx_boot1)*100,mean(cx_boot2)*100,cx_frac2*100,std(cx_boot2)*100,cx_p,1-cx_p)));

    
    % p14 p28 som
    cx_cmpdata = [cx_dataExtract{cx_tc,6}{1}-1,cx_dataExtract{cx_tc,6}{2}-1];   %  p14d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,7}{1}-1,cx_dataExtract{cx_tc,7}{2}-1];   %  p14d2
    cx_boot1 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac1 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    figure,histogram(cx_boot1),hold on
    
    cx_cmpdata = [cx_dataExtract{cx_tc,8}{1}-1,cx_dataExtract{cx_tc,8}{2}-1];   %  p28
    cx_boot2 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac2 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    histogram(cx_boot2),hold on;
    
    cx_comp = double(repmat(cx_boot1,[1 cx_nbootstrp]) >=  repmat(cx_boot2',[cx_nbootstrp 1]));
    if sum(cx_comp(:))>0
        cx_p = sum(cx_comp(:))/cx_nbootstrp^2;
    else
        cx_p = 1/cx_nbootstrp^2;
    end
    
    title(sprintf('p14 vs p28 %s frac, orig datasets\n %s',...
        cx_targetTypes{cx_tc},...
        sprintf('%.4f (%.4f) +- %.4f %% vs %.4f (%.4f) +- %.4f %%, p=%d (%d)',mean(cx_boot1)*100,cx_frac1*100,std(cx_boot1)*100,mean(cx_boot2)*100,cx_frac2*100,std(cx_boot2)*100,cx_p,1-cx_p)));
    
    % SYNAPSE DENSITIES
    
    % p7 p9 
    cx_cmpdata = [cx_dataExtract{cx_tc,1}{2},cx_dataExtract{cx_tc,1}{3}];   % som p7d1
    cx_boot1 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac1 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    figure,histogram(cx_boot1),hold on
    
    cx_cmpdata = [cx_dataExtract{cx_tc,3}{2},cx_dataExtract{cx_tc,3}{3}];   % som p9d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,4}{2},cx_dataExtract{cx_tc,4}{3}];   % som p9d2
    cx_boot2 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac2 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    histogram(cx_boot2),hold on;
    
    cx_comp = double(repmat(cx_boot1,[1 cx_nbootstrp]) >=  repmat(cx_boot2',[cx_nbootstrp 1]));
    %test: %cx_comp = uint8(repmat(cx_boot1,[1 cx_nbootstrp]) >=  repmat(cx_boot1'+(rand(1,cx_nbootstrp)-0.5)*mean(cx_boot1),[cx_nbootstrp 1]));
    cx_p = sum(cx_comp(:))/cx_nbootstrp^2;
    title(sprintf('p7 vs p9 %s syn dens, orig datasets\n %s',...
        cx_targetTypes{cx_tc},...
        sprintf('%.4f (%.4f) +- %.4f /µm vs %.4f (%.4f) +- %.4f /µm, p=%d (%d)',mean(cx_boot1),cx_frac1,std(cx_boot1),mean(cx_boot2),cx_frac2,std(cx_boot2),cx_p,1-cx_p)));
    
    
    % p9 p14 
    cx_cmpdata = [cx_dataExtract{cx_tc,3}{2},cx_dataExtract{cx_tc,3}{3}];   %  p9d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,4}{2},cx_dataExtract{cx_tc,4}{3}];   %  p9d2
    cx_boot1 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac1 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    figure,histogram(cx_boot1),hold on
    
    cx_cmpdata = [cx_dataExtract{cx_tc,6}{2},cx_dataExtract{cx_tc,6}{3}];   %  p14d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,7}{2},cx_dataExtract{cx_tc,7}{3}];   %  p14d2
    cx_boot2 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac2 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    histogram(cx_boot2),hold on;
    
    cx_comp = double(repmat(cx_boot1,[1 cx_nbootstrp]) >=  repmat(cx_boot2',[cx_nbootstrp 1]));
    cx_p = sum(cx_comp(:))/cx_nbootstrp^2;
    title(sprintf('p9 vs p14 %s syn dens, orig datasets\n %s',...
        cx_targetTypes{cx_tc},...
        sprintf('%.4f (%.4f) +- %.4f /µm vs %.4f (%.4f) +- %.4f /µm, p=%d (%d)',mean(cx_boot1),cx_frac1,std(cx_boot1),mean(cx_boot2),cx_frac2,std(cx_boot2),cx_p,1-cx_p)));
    
    
    % p14 p28 
    cx_cmpdata = [cx_dataExtract{cx_tc,6}{2},cx_dataExtract{cx_tc,6}{3}];   %  p14d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,7}{2},cx_dataExtract{cx_tc,7}{3}];   %  p14d2
    cx_boot1 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac1 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    figure,histogram(cx_boot1),hold on
    
    cx_cmpdata = [cx_dataExtract{cx_tc,8}{2},cx_dataExtract{cx_tc,8}{3}];   %  p28
    cx_boot2 = bootstrp(cx_nbootstrp,@cx_getFrac,cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_frac2 = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    histogram(cx_boot2),hold on;
    
    cx_comp = double(repmat(cx_boot1,[1 cx_nbootstrp]) >=  repmat(cx_boot2',[cx_nbootstrp 1]));
    cx_p = sum(cx_comp(:))/cx_nbootstrp^2;
    
    title(sprintf('p14 vs p28 %s syn dens, orig datasets\n %s',...
        cx_targetTypes{cx_tc},...
        sprintf('%.4f (%.4f) +- %.4f /µm vs %.4f (%.4f) +- %.4f /µm, p=%d (%d)',mean(cx_boot1),cx_frac1,std(cx_boot1),mean(cx_boot2),cx_frac2,std(cx_boot2),cx_p,1-cx_p)));
    
    
    
end


%mh_saveallfigs(fullfile(cx_path,'Table1_allStatistics_matlabFigs'));


%% plot densities p7 p9 Fig. 4F (from tabulated values)
% using: som (p7, p9, p14): 0.168+-0.014 ->> 0.088+-0.009
% AD (P7, P9, p14): 0.099+-0.013 ->> 0.075+-0.006

cx_bulk_som = [0.168 0.088 0.132]
cx_bulk_som_sem = [0.014 0.009 0.007]

cx_bulk_AD = [0.099 0.075 0.11]
cx_bulk_AD_sem = [0.013 0.006 0.006]

figure
plot(cx_bulk_som,'-m','LineWidth',5);hold on
plot(cx_bulk_som+cx_bulk_som_sem,'-m','LineWidth',1);hold on
plot(cx_bulk_som-cx_bulk_som_sem,'-m','LineWidth',1);hold on

plot(cx_bulk_AD,'-c','LineWidth',5);hold on
plot(cx_bulk_AD+cx_bulk_AD_sem,'-c','LineWidth',1);hold on
plot(cx_bulk_AD-cx_bulk_AD_sem,'-c','LineWidth',1);hold on

set(gca,'YLim',[0 0.2],'XLim',[0.9 2.1]);

box off
set(gca,'TickDir','out')

%% Multiplicity cross-check

cx_path = 'data'

cx_data = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'),3);

cx_rowSels{1} = {[1:21],[24:53,54:75],[77:111,112:143],[145:164]} % soma p7d1 p9d1 p9d2 p14d1 p14d2 p28
cx_rowSels{2} = {[167:186],[188:237,238:246],[248:314,315:363],[365:387]} % AD p7d1 p9d1 p9d2 p14d1 p14d2 p28
cx_timepoints = [7 9 14 28]
cx_targets= {'Soma','AD'}
cx_datasave={};
figure
for i=1:2
    subplot(1,2,i)
    for ii=1:length(cx_rowSels{i})
        cx_thisdata = cx_data(cx_rowSels{i}{ii},[5:6]);
        plot(cx_timepoints(ii),cx_thisdata(:,1)./cx_thisdata(:,2),'xk');hold on
        %plot(cx_timepoints(ii),sum(cx_thisdata(:,1))/sum(cx_thisdata(:,2)),'or','MarkerSize',10);
        cx_datasave{i,ii,1} = cx_thisdata(:,1)./cx_thisdata(:,2);
        cx_datasave{i,ii,2} =sum(cx_thisdata(:,1))/sum(cx_thisdata(:,2));
    end
    plot(cx_timepoints,cell2mat(cx_datasave(i,:,2)),'-r.','MarkerSize',20);
    set(gca,'YLim',[0 8.5]);set(gca,'XLim',[0 30]);box off;set(gca,'TickDir','out');
    set(gca,'YTick',[0:10],'XTick',[7 9 14 28]);
end


for i=1:2
    for ii=1:length(cx_rowSels{i})-1
        figure
        histogram(cx_datasave{i,ii,1});hold on
        histogram(cx_datasave{i,ii+1,1});hold on
        [p,h] = ranksum(cx_datasave{i,ii,1},cx_datasave{i,ii+1,1});
        title(sprintf('%s targets multiplicity P%d vs P%d\n %.2f +- %.2f, n=%d vs %.2f +- %.2f, n=%d , p=%d (ks2)',cx_targets{i},cx_timepoints(ii:ii+1),...
            cx_datasave{i,ii,2},std(cx_datasave{i,ii,1})/sqrt(length(cx_datasave{i,ii,1})),length(cx_datasave{i,ii,1}),...
            cx_datasave{i,ii+1,2},std(cx_datasave{i,ii+1,1})/sqrt(length(cx_datasave{i,ii+1,1})),length(cx_datasave{i,ii+1,1}),...
            p));
    end
end
           


mh_saveallfigs(fullfile('Multiplicity_figs'),'png');

%% Syn per soma cross-check

cx_path = 'data'

cx_data = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'),4);

cx_rowSels{1} = {[1:5],[7:11],[13:17],[19:23]} % soma 7 9 14 28
cx_timepoints = [7 9 14 28]
figure
i=1
for ii=1:length(cx_rowSels{i})
    cx_thisdata = cx_data(cx_rowSels{i}{ii},[11]);
    plot(cx_timepoints(ii),cx_thisdata,'xk');hold on
    if ii<length(cx_rowSels{i})
        cx_nextdata = cx_data(cx_rowSels{i}{ii+1},[11]);
        [p,h] = ranksum(cx_thisdata,cx_nextdata);
        sprintf('P%d: %.2f +- %.2f, n=%d versus P%d:  %.2f +- %.2f, n=%d\np=%d',cx_timepoints(ii),mean(cx_thisdata),std(cx_thisdata)/sqrt(length(cx_thisdata)),length(cx_thisdata),...
            cx_timepoints(ii+1),mean(cx_nextdata),std(cx_nextdata)/sqrt(length(cx_nextdata)),length(cx_nextdata),...
            p)
    end
end

% 
% 
% ans =
% 
%     'P7: 5.20 +- 0.97, n=5 versus P9:  8.00 +- 0.45, n=5
%      p=3.968254e-02'
% 
% 
% ans =
% 
%     'P9: 8.00 +- 0.45, n=5 versus P14:  47.00 +- 2.02, n=5
%      p=7.936508e-03'
% 
% 
% ans =
% 
%     'P14: 47.00 +- 2.02, n=5 versus P28:  75.20 +- 3.20, n=5
%      p=7.936508e-03'



%% re-plot Fig. 3b, 3d including P5 for final revision, using same numbers as in Table 1

%% generate table bootstrap comparison numbers and plots

% needs first section for data collection


cx_targetTypes = {'Soma','AD'}
figure
cx_noise=0.6;
cx_boxw=0.8;
cx_bulks=[];
for cx_tc=1:length(cx_targetTypes)
    subplot(1,2,cx_tc);
    
    % p7
    cx_cmpdata = [cx_dataExtract{cx_tc,1}{1}-1,cx_dataExtract{cx_tc,1}{2}-1];   %  p7d1
    %cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,2}{1}-1,cx_dataExtract{cx_tc,2}{2}-1];   %  p7d2
    cx_fractions = cx_cmpdata(:,1)./cx_cmpdata(:,2);
    %plot(7,cx_fractions,'xk'); hold on;
    cx_plotFrac_N(7,cx_fractions,cx_cmpdata(:,2),cx_noise)
    cx_bulks(cx_tc,2) = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_BoxPlot(7,cx_fractions,cx_boxw)
    
    % p9
    cx_cmpdata = [cx_dataExtract{cx_tc,3}{1}-1,cx_dataExtract{cx_tc,3}{2}-1];   % p9d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,4}{1}-1,cx_dataExtract{cx_tc,4}{2}-1];   %  p9d2
    %cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,5}{1}-1,cx_dataExtract{cx_tc,5}{2}-1];   %  p9d3
    cx_fractions = cx_cmpdata(:,1)./cx_cmpdata(:,2);
    %plot(9,cx_fractions,'xk'); hold on;
    cx_plotFrac_N(9,cx_fractions,cx_cmpdata(:,2),cx_noise)
    cx_bulks(cx_tc,3) = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_BoxPlot(9,cx_fractions,cx_boxw)
    
    % p14
    
    cx_cmpdata = [cx_dataExtract{cx_tc,6}{1}-1,cx_dataExtract{cx_tc,6}{2}-1];   %  p14d1
    cx_cmpdata = [cx_cmpdata;cx_dataExtract{cx_tc,7}{1}-1,cx_dataExtract{cx_tc,7}{2}-1];   %  p14d2
    cx_fractions = cx_cmpdata(:,1)./cx_cmpdata(:,2);
    %plot(14,cx_fractions,'xk'); hold on;
    cx_plotFrac_N(14,cx_fractions,cx_cmpdata(:,2),cx_noise)
    cx_bulks(cx_tc,4) = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_BoxPlot(14,cx_fractions,cx_boxw)
    
    % p28
    cx_cmpdata = [cx_dataExtract{cx_tc,8}{1}-1,cx_dataExtract{cx_tc,8}{2}-1];   %  p28
    cx_fractions = cx_cmpdata(:,1)./cx_cmpdata(:,2);
    %plot(28,cx_fractions,'xk'); hold on;
    cx_plotFrac_N(28,cx_fractions,cx_cmpdata(:,2),cx_noise)
    cx_bulks(cx_tc,5) = cx_getFrac(cx_cmpdata(:,1),cx_cmpdata(:,2));
    cx_BoxPlot(28,cx_fractions,cx_boxw)
    
end


% add P5 data:
cx_syndata = xlsread(fullfile(cx_path,'P5_L4_reannotation_inclNewAnnot.xlsx'));

cx_thisrowrange = [1:7]; % p5 AD
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [22:24,26:27]
cx_colrange = [22:24]   %AD incl seed cl 1+2
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
subplot(1,2,2);
% plot(5,cx_fractions,'xk'); hold on;
cx_plotFrac_N(5,cx_fractions,cx_ns,cx_noise)
cx_bulks(2,1) = cx_getFrac(sum(cx_thisdata(:,cx_colrange),2)-1,sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_BoxPlot(5,cx_fractions,cx_boxw)

cx_thisrowrange = [14:25]; % p5 som
cx_thisdata = cx_syndata(cx_thisrowrange,:);
cx_colrange_allsyn = [22:24,26:27]
cx_colrange = [22:24]   %som incl seed cl 1+2
cx_fractions = (sum(cx_thisdata(:,cx_colrange),2)-1)./(sum(cx_thisdata(:,cx_colrange_allsyn),2)-1);
cx_ns = sum(cx_thisdata(:,cx_colrange_allsyn),2);
subplot(1,2,1);
% plot(5,cx_fractions,'xk'); hold on;
cx_plotFrac_N(5,cx_fractions,cx_ns,cx_noise)
cx_bulks(1,1) = cx_getFrac(sum(cx_thisdata(:,cx_colrange),2)-1,sum(cx_thisdata(:,cx_colrange_allsyn),2)-1); 
cx_BoxPlot(5,cx_fractions,cx_boxw)

subplot(1,2,1);
set(gca,'XLim',[0 30]);
set(gca,'YLim',[0 1]);
%plot([5 7 9 14 28],cx_bulks(1,:),'ok-','MarkerSize',10,'LineWidth',4);
plot([5 7 9 14 28],cx_bulks(1,:),'k-','LineWidth',4);
box off; set(gca,'TickDir','out','XTick',[5 7 9 14 28]);
subplot(1,2,2);
set(gca,'XLim',[0 30]);
set(gca,'YLim',[0 1]);
%plot([5 7 9 14 28],cx_bulks(2,:),'ok-','MarkerSize',10,'LineWidth',4);
plot([5 7 9 14 28],cx_bulks(2,:),'k-','LineWidth',4);
box off; set(gca,'TickDir','out','XTick',[5 7 9 14 28]);

%% getFrac function, now in extra .m file

% function cx_f = cx_getFrac(cx_on,cx_tot)
%     cx_f = sum(cx_on)/sum(cx_tot);
% end
